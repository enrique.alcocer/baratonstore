![Rappi](http://blogs.eltiempo.com/digital-jumper/wp-content/uploads/sites/483/2017/07/FOTO-WEB-RAPPI-810x410.png)

# BaratonStore

Proyecto creado como parte de Frontend Developer Challenge de Rappi

## Getting Started
Tiendas “El Baratón” necesita un e-commerce para expandir sus servicios, para eso don
Pepe (propietario de la tienda) ha provisto de los siguientes requerimientos:
* Crear un menu con Categorias anidadas
* Los productos deben filtrarse por: disponibilidad, rango de precios, cantidad en
stock
* Los productos deben poder ordenarse por precio, disponibilidad y cantidad.
* Se debe crear un carrito de compras donde los usuarios puedan agregar, editar
cantidad y eliminar un producto.
* Los productos deben permanecer en el carrito si el usuario cierra y abre la página,
solo deben ser borrados si el usuario realiza la compra.
* Un subnivel final es aquel que no tiene más subniveles, en éste caso debe aparecer
una caja de texto que permita realizar búsquedas de productos por nombre en
dichos subniveles.
* El ecommerce debe ser responsive.

### Prerequisites

El proyecto fue desarrollado con las siguientes tecnologias:

* [Angular versión 8](http://www.dropwizard.io/1.0.2/docs/) 
* [Angular-CLI versión 8.0.2](https://cli.angular.io/) 
* [Node.js versión 10.16.0 (includes npm 6.9.0) ](https://nodejs.org/es/download/) 
* [Git versión 2.22.0](https://git-scm.com/download/win) 
* [Visual Studio Code versión 1.35](https://code.visualstudio.com/) 

Fueron usadas las siguientes librerías:
* [popper.js versión  1.15.0](https://www.npmjs.com/package/popper.js/v/1.15.0)
* [Boostrap versión 4.3.1](https://www.npmjs.com/package/bootstrap) 
* [Angular fontawesome versión 3.1.2](https://www.npmjs.com/package/angular-font-awesome)
* [jQuery versión 3.4.1](https://www.npmjs.com/package/jquery)
* [font-awesome versión 4.7.0](https://www.npmjs.com/package/angular-font-awesome)
* [Underscore js versión 1.9.1](https://www.npmjs.com/package/underscore)
* [Angular-web-storage versión 7.0.0](https://www.npmjs.com/package/angular-web-storag)


### Installing

Instalar angular-cli:
```
npm i -g @angular/cli@latest
```
Instalar popper.js:
```
npm install --save popper.js angular-popper
```
Instalar Jquery:
```
npm install --save jquery
```
Instalar Boostrap 4: 
```
npm install --save bootstrap
```
Instalar angular-font-awesome: 
```
npm install --save font-awesome angular-font-awesome
```
Instalar font-awesome:
```
npm install --save font-awesome angular-font-awesome
```
Instalar underscore:
```
npm install underscore
```
Instalar angular-web-storage:
```
 npm install angular-web-storage --save
```

O simplemente puede instalar todos los modulos ejecutando:
```
 npm install
```


## Running the app

En una terminal, ubicarse en la carpeta del proyecto y ejecutar:
```
ng serve -o
```
la url de la aplicación se abre en un navegador con la url http://localhost:4200/
## Deployment



## Authors

* **Enrique Alcocer** 

## Acknowledgments

Para solucionar el problema del menu con categorias anidadas, se uso conceptos de recursividad. Para almacenar los items del carrito de compra se trabajo con local storage, ofrecido en navegadores modernos