import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ProductListComponent } from './product-list/product-list.component';

const routes: Routes = [
  { path: 'welcome',  component: WelcomeComponent},
  { path: '', redirectTo:"welcome", pathMatch:"full"},
  { path: 'product-list/:id',  component: ProductListComponent},
  { path: 'product-list',redirectTo:"product-list/:id", pathMatch:"full"},
  { path: 'cart',  component: CartComponent},  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
