import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../services/category/category.service';
import { ProductService } from '../services/product/product.service';
import { ProductListComponent } from '../product-list/product-list.component';
import { CartService } from '../services/cart/cart.service';




@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  filterName: string;
  constructor(
    private categoryService: CategoryService,
    private cartService:CartService
  ) {


  }

  ngOnInit() {
  }
  

  getQuantityProductToBuy():number{
   return this.cartService.products.length;
  }

  getActiveCategories() {
    return this.categoryService.categories;
  }

}
