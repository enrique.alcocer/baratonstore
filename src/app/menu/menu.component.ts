import { Component, OnInit, Input } from '@angular/core';
import { Category } from '../model/Category';
import { ProductService } from '../services/product/product.service';
import { CategoryService } from '../services/category/category.service';

@Component({
  selector: '[menu-component]',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  @Input() categories:Array<Category>;
  constructor(private productService:ProductService, 
    private categoryService:CategoryService) { }

  ngOnInit() {
  }

  
  

}
