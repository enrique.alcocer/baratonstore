export interface Category {

    id: Number;
    name: String;
    sublevels?: Array<Category>; 
    parent?:Category;
 
}