export interface Product {
 
    id: string;
    name:string;    
    price:string;
    quantity:number;
    sublevel_id:number;
    available:false;
    priceValue:number; 
    quantityToBuy:number;   
 
 
}