import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../model/product';
import { CartService } from '../services/cart/cart.service';

@Component({
  selector: '[product-component]',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  @Input() product:Product;

  constructor(private cartService:CartService) { }

  ngOnInit() {
  }

  addToCart(product:Product):void{
     this.cartService.addProduct(product);
  }
}
