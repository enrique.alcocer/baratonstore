import { Component, OnInit, Input } from '@angular/core';
import { CategoryService } from '../services/category/category.service';
import { ProductService } from '../services/product/product.service';
import { Category } from '../model/Category';


@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  @Input() activedCategory:Category;
  constructor(private categoryService:CategoryService,
    private productService:ProductService) { }

  ngOnInit() {
  }

  getAncestors(category:Category){
    console.log("category:"+category);
    return this.categoryService.getAncestorsCategory(category,[]).reverse();
  }  
  
}
