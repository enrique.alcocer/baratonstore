import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart/cart.service';
import { Product } from '../model/Product';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  constructor(private cartService:CartService) { }

  ngOnInit() {
  }

  getProducts():Array<Product>{
    return this.cartService.products;
  }

  changeQuantityProduct(value:number,product:Product):void{
    if(value<=0 && product.quantityToBuy==1) return;
    product.quantityToBuy+=value;
    this.cartService.saveLocalStorage();
  }

  removeProduct(product:Product):void{
    this.cartService.removeProduct(product);
  }
  getTotal():String{
    var total:number=0;
    this.cartService.products.forEach(product => {
      total+=product.priceValue;
    });

    return "$"+total.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'); 
  }

  checkout():void{
    this.cartService.removeAll();
  }
}
