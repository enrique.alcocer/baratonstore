import { Component, OnInit, Injectable } from '@angular/core';
import { CategoryService } from '../services/category/category.service';
import { ProductService } from '../services/product/product.service';
import { Category } from '../model/Category';
import { Observable } from 'rxjs';
import { Product } from '../model/product';
import { ActivatedRoute, Params } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {


  activedCategory: Category;
  activedProducts: Array<Product>;


  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private activatedRoute: ActivatedRoute) {


  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      var activedCategoryId = Number.parseInt(params['id']);
      var filterName = params['filterName'];
      if (filterName) {
        this.activedCategory=null;
        this.activedProducts = this.productService.filterProductsDeepByName(filterName);
      }
      else {
        this.activedCategory = this.categoryService.findCategoryById(activedCategoryId, this.categoryService.categories);
        this.activedProducts = this.productService.filterProductsByCategory(activedCategoryId);
      }
      return params;
    });
  }




}
