import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product/product.service';
import { ProductListComponent } from '../product-list/product-list.component';
import { Product } from '../model/Product';


@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})

export class FilterComponent implements OnInit {

  minValue: Number;
  maxValue: Number;
  quantity: Number;
  available: Boolean;
  errorFilterMessages: Array<String> = [];
 

  orderFields: Array<any> = [
    { name: "Precio", value: ProductService.ORDER_PRICE },
    { name: "Disponibilidad", value: ProductService.ORDER_AVAILABLE },
    { name: "Cantidad", value: ProductService.ORDER_QUANTITY }];
  constructor(
    private productService: ProductService,
    private productListComponent: ProductListComponent
  ) {
  }

  ngOnInit() {
  }

  filterProducts(): void {
    this.errorFilterMessages = [];
    if (this.minValue > this.maxValue) {
      this.errorFilterMessages.push("Valores no validos para rango de precios");
      return;
    }
    
    var activeProducts:Array<Product>=this.productListComponent.activedCategory?
    this.productService.filterProductsByCategory(this.productListComponent.activedCategory.id)
    :this.productListComponent.activedProducts;
    

    this.productListComponent.activedProducts =
      this.productService.filterProductByFilters(activeProducts,this.minValue, this.maxValue, this.quantity, this.available);
  }

  orderProducts(orderField: String): void {
    this.productListComponent.activedProducts =
      this.productService.orderProducts(this.productListComponent.activedProducts, orderField);
  }  

}
