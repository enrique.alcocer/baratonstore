import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Category } from '../../model/Category';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const CATEGORY_URL = environment.categoryURL;


@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  

  categories: Category[];

  

  constructor(private http: HttpClient) { }


  getCategories(): Observable<Category[]> {

    return this.http.get<Category[]>(CATEGORY_URL)
      .pipe(
        map(res => res['categories'])
      );
  }


  findCategoryById(categoryId: Number, categories: Array<Category>): Category {

    if (!categories) return null;
    var categoryFound: Category;
    categories.forEach(category => {
      if (category.id === categoryId) {
        categoryFound = category;
      }
      else if (!categoryFound && category.sublevels) {
        categoryFound = this.findCategoryById(categoryId, category.sublevels);
      }
    });
    return categoryFound;

  }  

  fillParent(parent: Category, categories: Category[]): void {
    if (!categories) return;
    categories.forEach(category => {
      category.parent = parent;
      this.fillParent(category, category.sublevels);
    });
  }

  getAncestorsCategory(category: Category, ancestors: Array<Category>): Array<Category> {
    if (category != null) {
      ancestors.push(category);
      this.getAncestorsCategory(category.parent, ancestors);
    }

    return ancestors;
  }

  findCategoriesDeep(categories: Array<Category>, categoriesDeep: Array<Category>): void {

    categories.forEach(category => {
      if (category.sublevels) {
        this.findCategoriesDeep(category.sublevels, categoriesDeep);
      }
      else {
        categoriesDeep.push(category);
      }
    });

  }






}