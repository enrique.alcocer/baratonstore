import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Product } from '../../model/Product';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { _ } from 'underscore';
import { CategoryService } from '../category/category.service';
import { Category } from 'src/app/model/Category';

const PRODUCT_URL = environment.productsURL;


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  static ORDER_PRICE: String = "priceValue";
  static ORDER_QUANTITY: String = "quantity";
  static ORDER_AVAILABLE: String = "available";
  products: Product[]
 
  constructor(private http: HttpClient, private categoryService: CategoryService) { }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(PRODUCT_URL)
      .pipe(
        map(res => res['products'])
      );
  }

  fillPriceValue(products: Array<Product>): void {
    if (!products) return;
    products.forEach(product =>
      product.priceValue = Number.parseInt(product.price.replace("$", '').replace(",", ''))
    );
  }

  filterProductsByCategory(categoryId: Number): Array<Product> {
    if (!this.products) return [];

    var activeProducts:Array<Product>  = this.products.filter(
      product => product.sublevel_id === categoryId);

    return activeProducts;
  }

  getProductsByCategory(categoryId: Number): Array<Product> {

    return this.products.filter(
      product => product.sublevel_id === categoryId);


  }

  filterProductByFilters(activeProducts: Array<Product>, minValue: Number, maxValue: Number, quantity: Number, available: Boolean): Array<Product> {

    if (!activeProducts) return [];

    if (minValue > 0 && maxValue > 0) {
      activeProducts = activeProducts.filter(product => this.existsProductByRangePrice(product, minValue, maxValue));
    }
    if (quantity > 0) {
      activeProducts = activeProducts.filter(product => product.quantity === quantity);
    }
    if (available) {
      activeProducts = activeProducts.filter(product => product.available);
    }

    return activeProducts;

  }

  private existsProductByRangePrice(product: Product, minValue: Number, maxValue: Number): boolean {
    var productPrice: Number = product.price ? Number.parseInt(product.price.replace("$", '').replace(",", '')) : -1;
    return productPrice >= minValue && productPrice <= maxValue;
  }

  orderProducts(products: Array<Product>, orderField: String): Array<Product> {
    if (!products) return;
    return _.sortBy(products, orderField);
  }

  filterProductsDeepByName(value: string): Array<Product> {
    var productsFound: Array<Product> = [];
    var categoriesDeep: Array<Category> = [];
    this.categoryService.findCategoriesDeep(this.categoryService.categories, categoriesDeep);
    categoriesDeep.forEach(category => {
      var products: Array<Product> = this.getProductsByCategory(category.id);
      products.forEach(product => {
        if (product.name.indexOf(value) >= 0) {
          productsFound.push(product);
        }
      });

    });

    return productsFound;
  }


}
