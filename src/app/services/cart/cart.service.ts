import { Injectable } from '@angular/core';
import { Product } from 'src/app/model/Product';
import { _ } from 'underscore';
import { LocalStorageService } from 'angular-web-storage';

@Injectable({
  providedIn: 'root'
})
export class CartService {
 

  KEY = 'products-baraton-store';
  products: Array<Product> = [];
  constructor(private localStorage: LocalStorageService) {
    var localProducts = this.localStorage.get(this.KEY);
    if (localProducts) {
      this.products = JSON.parse(localProducts);
    }

  }

  addProduct(productToAdd: Product) {
    var product = this.findProductByid(productToAdd.id);
    if (!product) {
      productToAdd.quantityToBuy = 1;
      this.products.push(productToAdd);
    }
    else {
      product.quantityToBuy++;
    }
    this.saveLocalStorage();

  }

  private findProductByid(productId: String): Product {
    var products: Array<Product> = this.products.filter(product => product.id === productId);
    return products.length > 0 ? products[0] : null;
  }

  removeProduct(productToRemove: Product): void {
    this.products = _.without(this.products, productToRemove);
    this.saveLocalStorage();
   
  }
  removeAll() {
    this.products=[];
    this.localStorage.remove(this.KEY);
  }
  saveLocalStorage(){
    this.localStorage.set(this.KEY, JSON.stringify(this.products), 72, 'h');
  }
}
