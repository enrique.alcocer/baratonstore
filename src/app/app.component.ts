import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from './model/Category';
import { CategoryService } from './services/category/category.service';
import { ProductService } from './services/product/product.service';
import { Product } from './model/product';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'BaratonStore';

  constructor(private categoryService: CategoryService, private productService: ProductService) {

  }
  ngOnInit() {
    const obsCategories: Observable<Category[]> = this.categoryService.getCategories();
    obsCategories.subscribe((categories: Category[]) => {
      console.log('categories:' + categories.length);
      this.categoryService.categories = categories;  
      this.categoryService.fillParent(null, categories);

      const obsProducts: Observable<Product[]> = this.productService.getProducts();
      obsProducts.subscribe((products: Product[]) => {
        this.productService.products = products;
        this.productService.fillPriceValue(products);
      },
        error => console.error('product-list > error load products', error));
    },
      error => console.error('nav-bar > error load categories', error));

  }

}
